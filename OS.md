System calls are similar to function calls, but they are not the same. System calls need to satisfy a few more requirements 
in order to let the operating system know how it needs to interact with the processor in order to execute a program. System
calls need to exist for the operating system to use the processor to execute a function. Functions are just a series of
system calls that allow the user to care less about what is happening under the system. To run correctly, system calls need
a few registers to know what the system call needs to do and where its resources lie. In ARM, r0-r7 is reserved for system 
calls. r7 in particular is reserved for what type of system call is being called, while other registers are used as arguments.
